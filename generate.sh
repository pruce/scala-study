#!/bin/sh
mkdir -p src/{main,test}/{java,resources,scala}
mkdir lib project target
touch project/Build.scala
# create an initial build.sbt file
cat <<EOF > build.sbt
name := "sbtProject"
version := "1.0"
scalaVersion := "2.10.5"
EOF